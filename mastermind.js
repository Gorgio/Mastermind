// Game board and status
const root = document.documentElement;
const gameBoard = document.querySelector('.game-board');
let playStatus = false;
let playerTurn = 1;
let combinationChoicesDone = 0;
let mysteryCombination = [];
toggleVisibilityGameActions();

function toggleVisibilityGameActions() {
    let gameActions = document.getElementById('game-actions');
    if (gameActions.style.display === 'none') {
        gameActions.style.display = 'block';
    } else gameActions.style.display = 'none';
}

function addButtonsEventHandler(nodes, cssVariable) {
    for (let i = 0; i < nodes.length; i++) {
        let node = nodes[i];
        node.addEventListener('click', () => {
            if (playStatus === false) {
                node.parentNode.querySelector('.current').classList.remove('current');
                node.classList.add('current');
                root.style.setProperty(cssVariable, node.value, '');
            }
        });
    }
}

// Set how many attempts the player has
addButtonsEventHandler(document.querySelectorAll('.try-button'), '--rows');
// Set the size of the combination
addButtonsEventHandler(document.querySelectorAll('.combination-size-button'), '--cols');
// Set how many colors the combination can have.
addButtonsEventHandler(document.querySelectorAll('.color-choice-button'), '--colorChoice');
// Event handler for color combination selection.
const colors = [].slice.call(document.querySelectorAll('.select-button'));
for (let i = 0; i < colors.length; i++) {
    let color = colors[i];
    color.addEventListener('click', () => {
        if (playStatus === true && (combinationChoicesDone < getComputedStyle(root).getPropertyValue('--cols'))) {
            gameBoard.insertAdjacentHTML('beforeend', '<div class="game-button" data-type="answer" data-value="' + color.value + '"></div>');
            combinationChoicesDone++
        }
    });
}
// Set Event Handler for reset last move
const resetLastMoveButton = document.querySelector('.reset-last-move');
resetLastMoveButton.addEventListener('click', () => {
    if (combinationChoicesDone > 0) {
        gameBoard.removeChild(gameBoard.lastChild);
        combinationChoicesDone--
    }
});
// Set event handler for submission of combination
const submitCombinationButton = document.querySelector('.submit-combination');
submitCombinationButton.addEventListener('click', () => {
    if (combinationChoicesDone === parseInt(getComputedStyle(root).getPropertyValue('--cols'), 10)) {
        submissionSubRoutine()
    }
});

// Add event listener on the play/stop button to change the play status
const playStopButton = document.getElementById('play-stop-button');
playStopButton.addEventListener('click', changePlayStatus);
// Change game status (play/stop)
function changePlayStatus() {
    playStatus = !playStatus;
    toggleVisibilityGameActions();
    if (playStatus === true) {
        playStopButton.textContent = "Stop";
        const gameActionsPanel = document.querySelector('.game-board-actions');
        while (gameActionsPanel.children.length > 1) {
            gameActionsPanel.removeChild(gameActionsPanel.firstChild);
        }
        while (gameBoard.hasChildNodes()) {
            gameBoard.removeChild(gameBoard.lastChild);
        }
        generateRandomCombination();
        playerTurn = 1;
        combinationChoicesDone = 0;
    } else {
        playStopButton.textContent = "Play"
    }
}

function generateRandomCombination() {
    let colorChoicesElements =  colors.slice(0, parseInt(getComputedStyle(root).getPropertyValue('--colorChoice'), 10));
    let colorChoices = [];
    mysteryCombination = [];
    for (let i = 0; i < colorChoicesElements.length; i++) {
        colorChoices.push(colorChoicesElements[i].value);
    }
    for (let j = 0; j < getComputedStyle(root).getPropertyValue('--cols'); j++) {
        // Only Uniform distribution because we have 0 as our minimum integer
        mysteryCombination.push(colorChoices[Math.floor(Math.random()*colorChoices.length)])
    }
}

function submissionSubRoutine() {
    let correctGuesses = getCorrectGuesses();
    const answerHtml = `<div class="combination-answer">
    <label for="correct-position-turn-${playerTurn}">Pins in the correct position: </label>
    <output id="correct-position-turn-${playerTurn}">${correctGuesses.position}</output><br>
    <label for="correct-colour-turn-${playerTurn}">Pins in the correct colour: </label>
    <output id="correct-colour-turn-${playerTurn}">${correctGuesses.colour}</output>
</div>`;
    document.getElementById('game-actions').insertAdjacentHTML('beforebegin', answerHtml);
    combinationChoicesDone = 0;
    playerTurn++;
    if (correctGuesses.position === parseInt(getComputedStyle(root).getPropertyValue('--cols'), 10)) {
        gameBoard.insertAdjacentHTML('beforeend', '<p class="result">You Won</p>');
        changePlayStatus();
    } else if (
        playerTurn > parseInt(getComputedStyle(root).getPropertyValue('--rows'), 10) &&
        correctGuesses.position !== parseInt(getComputedStyle(root).getPropertyValue('--cols'), 10)
    ) {
        gameBoard.insertAdjacentHTML('beforeend', '<p class="result">You Lost</p>');
        // TODO display solution when player loses.
        changePlayStatus();
    }
}

function getLastCombinationAttempt() {
    let firstChild = (playerTurn-1)*getComputedStyle(root).getPropertyValue('--cols');
    let combinationChoicesValues = [];
    let childrenNodes = gameBoard.childNodes;
    for (let i = firstChild; i < childrenNodes.length; i++) {
        combinationChoicesValues.push(childrenNodes[i].dataset.value);
    }
    return combinationChoicesValues
}

function getCorrectGuesses() {
    let combinationAttempt = getLastCombinationAttempt();
    let result = { position: 0, colour: 0};
    if (JSON.stringify(combinationAttempt) === JSON.stringify(mysteryCombination)) {
        result.position = mysteryCombination.length;
        return result
    }
    if (combinationAttempt.length !== mysteryCombination.length) return result;
    for (let i = 0; i < mysteryCombination.length; i++) {
        if (mysteryCombination[i] === combinationAttempt[i]) {
            result.position = result.position + 1;
        } else {
            for (let j = 0; j < combinationAttempt.length; j++) {
                if (i !== j && combinationAttempt[j] === mysteryCombination[i]){
                    result.colour = result.colour + 1;
                    break;
                }
            }
        }
    }
    return result;
}